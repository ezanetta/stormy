# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/ezanetta/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep class butterknife.** { *; }
-keep class **$$ViewInjector { *; }
-keep class org.spongycastle.** { *; }
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-keep class retrofit.** { *; }
-keepattributes Signature
-keepattributes *Annotation*

-dontwarn okio.**
-dontwarn org.spongycastle.**
-dontwarn com.squareup.okhttp.**
-dontwarn retrofit.**
-dontwarn retrofit.appengine.UrlFetchClient
-dontwarn butterknife.internal.**
-dontwarn javax.naming.**

-keepnames class com.squareup.okhttp.** { *; }
-keepnames interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.internal.**
-dontwarn okio.**
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
    @retrofit.http.* <methods>;
    @butterknife.* <fields>;
}







